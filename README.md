# Connecting Rivers and Lakes FAIRly: Advancing GeoFRESH

## **Overview**
The **Connecting Rivers and Lakes FAIRly** pilot, led by the **Leibniz-Institute of Freshwater Ecology and Inland Fisheries (IGB)**, advances the integration of geospatial freshwater data. This project builds directly on the **GeoFRESH** initiative, enhancing its scope by linking lake data (e.g., HydroLAKES) with river networks (e.g., Hydrography90m), ensuring a seamless representation of freshwater systems for scientific and policy applications.

### **Contributors**
- **Lead Team**: Thomas Tomiczek, Vanessa Bremerich, Afroditi Grigoropoulou, Jaime Garcia Marquez, and others from IGB and partner institutions.
- Funded under **NFDI4Earth**, the project extends the functionality of GeoFRESH and integrates FAIR principles to maximize usability across disciplines.

## **How This Pilot Advances GeoFRESH**
1. **Broader Integration**:
   - GeoFRESH provided a foundational platform for river network analysis. This pilot incorporates lake datasets, harmonizing them with river data to create a unified freshwater continuum.
   - Integration of datasets like **HydroLAKES** and **Hydrography90m** ensures compatibility with emerging data sources, such as NASA’s SWOT mission.

2. **Enhanced Tools**:
   - Developed new R functions in the **hydrographr** package:
     - `extract_lake_ids()`: Identifies relevant lake IDs based on spatial criteria.
     - `get_lake_intersection()`: Determines intersection points between lakes and rivers.
     - `get_lake_catchment()`: Delineates lake catchment areas from these intersections.
   - These tools expand GeoFRESH functionalities, enabling users to analyze lakes and rivers simultaneously within the same workflow.

3. **Global Lake Catchment Database**:
   - Generated a comprehensive geospatial dataset for over 1.4 million lakes, providing key attributes like lake catchments, intersections with river networks, and outlet locations.
   - Data is accessible through the GeoFRESH platform, making it easier for users to visualize and analyze complex freshwater systems.

4. **Workflow Tutorials**:
   - Created detailed tutorials and vignettes to guide users in applying these new tools, ensuring accessibility for both beginners and advanced researchers.

## **Achievements**
- Successfully linked global river and lake datasets, addressing the challenge of spatial and functional disconnection.
- Released updates to **GeoFRESH** and **hydrographr**, enabling scalable, reproducible analyses of freshwater systems.
- Published tools and datasets under open-source licenses (e.g., [hydrographr GitHub](https://github.com/glowabio/hydrographr)).

## **Challenges**
- Computationally intensive tasks required iterative updates and extensive resources, highlighting the need for scalable infrastructure like HPC systems.
- Harmonizing diverse datasets (e.g., DEM-derived river networks and satellite-based lake polygons) demanded innovative processing methods and tools.

## **Future Directions**
1. **Integration with New Datasets**:
   - Prepare workflows for the upcoming **SWOT mission** data, which will provide a higher-resolution inventory of lakes and reservoirs.
   - Continuously curate global lake and river datasets to maintain relevance.

2. **User Accessibility**:
   - Enhance the GeoFRESH platform with intuitive interfaces for non-technical users.
   - Develop new tools and features to further streamline workflows for biodiversity and hydrology research.

3. **Interdisciplinary Applications**:
   - Extend tools to other domains, such as urban planning and climate resilience, where similar geospatial analysis workflows can be applied.

By seamlessly integrating lake and river data, this pilot builds on GeoFRESH’s foundation, transforming it into a comprehensive platform for analyzing and managing freshwater habitats globally. Explore tools and resources at [GeoFRESH](https://geofresh.org/) and [hydrographr GitHub](https://github.com/glowabio/hydrographr).
